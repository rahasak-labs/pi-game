import pygame
import random

from pygame.locals import (
    RLEACCEL,
    K_UP,
    K_DOWN,
    K_LEFT,
    K_RIGHT,
    K_ESCAPE,
    KEYDOWN,
    QUIT,
)

# colors
white = (255,255,255)
black = (0,0,0)
bright_red = (255,0,0)
bright_green = (0,255,0)
red = (200,0,0)
green = (0,200,0)
light_green = (43, 181, 80)

# screen size
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

# differnet documents
# differnt issues/bombs
documents = ['media/email.png', 'media/rainy.png', 'media/storm.png', 'media/cloud1.png']
enimies = ['media/bullet.png', 'media/missile.png', 'media/bomb.png']

# game score
score = 0

# bullet speed decide based on this time
time_elapsed = 0
bullet_speed = 1000

class Player(pygame.sprite.Sprite):
    def __init__(self):
        super(Player, self).__init__()
        self.surf = pygame.image.load("media/rocket.png")
        self.surf.set_colorkey((255, 255, 255), RLEACCEL)
        #self.rect = self.surf.get_rect()
        self.rect = self.surf.get_rect(
            center=(
                50,
                SCREEN_HEIGHT/2 - 10,
           )
        )

    def update(self, pressed_keys):
        if pressed_keys[K_UP]:
            self.rect.move_ip(0, -5)
            move_up_sound.play()
        if pressed_keys[K_DOWN]:
            self.rect.move_ip(0, 5)
            move_down_sound.play()
        if pressed_keys[K_LEFT]:
            self.rect.move_ip(-5, 0)
        if pressed_keys[K_RIGHT]:
            self.rect.move_ip(5, 0)

        if self.rect.left < 0:
            self.rect.left = 0
        elif self.rect.right > SCREEN_WIDTH:
            self.rect.right = SCREEN_WIDTH
        if self.rect.top <= 0:
            self.rect.top = 0
        elif self.rect.bottom >= SCREEN_HEIGHT:
            self.rect.bottom = SCREEN_HEIGHT

class Enemy(pygame.sprite.Sprite):
    def __init__(self):
        super(Enemy, self).__init__()
        self.surf = pygame.image.load(random.choice(enimies))
        self.surf.set_colorkey((255, 255, 255), RLEACCEL)
        self.rect = self.surf.get_rect(
            center=(
                random.randint(SCREEN_WIDTH + 20, SCREEN_WIDTH + 100),
                random.randint(0, SCREEN_HEIGHT),
           )
        )
        self.speed = random.randint(5, 20)

    def update(self):
        self.rect.move_ip(-self.speed, 0)
        if self.rect.right < 0:
            self.kill()

class Cloud(pygame.sprite.Sprite):
    def __init__(self):
        super(Cloud, self).__init__()
        self.surf = pygame.image.load(random.choice(documents))
        self.surf.set_colorkey((0, 0, 0), RLEACCEL)
        self.rect = self.surf.get_rect(
            center=(
                random.randint(SCREEN_WIDTH + 20, SCREEN_WIDTH + 100),
                random.randint(0, SCREEN_HEIGHT),
            )
        )
        self.speed = random.randint(1, 5)

    def update(self):
        self.rect.move_ip(-self.speed, 0)
        if self.rect.right < 0:
            self.kill()

# set score
def set_score():
    TextSurf, TextRect = text_objects("Score: " + str(score), small_text, black)
    TextRect.center = ((SCREEN_WIDTH - 70), 10)
    screen.blit(TextSurf, TextRect)

# set background
def set_bg():
    bg_img = pygame.image.load('media/bg.png')
    bg_img = pygame.transform.scale(bg_img,(SCREEN_WIDTH,SCREEN_HEIGHT))
    screen.blit(bg_img,(0,0))

def remove_collieds():
    for cloud in clouds:
        if pygame.sprite.spritecollideany(player, clouds):
            clouds.remove(cloud)
            cloud.kill()

def refresh_bullet_speed():
    dt = clock.tick()

    global time_elapsed
    global bullet_speed
    time_elapsed += dt
    if time_elapsed > 100:
        print("bullet speed " + str(bullet_speed))
        time_elapsed = 0
        if bullet_speed > 100:
            bullet_speed -= 100
            pygame.time.set_timer(ADDENEMY, bullet_speed)

# creatig object for the displaying of the messages i.e. surface and rectangle
def text_objects(text, font, color):
    textSurface = font.render(text,True,color)
    return textSurface, textSurface.get_rect()

# create button
def button(msg,x,y,w,h,ic,ac,action = None):
    # getting the position of the mouse
    mouse = pygame.mouse.get_pos()

    # getting the click of the mouse
    click = pygame.mouse.get_pressed()

    # checking if cursor on the button
    if x+w > mouse[0] > x and y + h > mouse[1] > y:
        pygame.draw.rect(screen, ac, (x,y,w,h))
        # change colour on hover and function according to the function object passes
        if click[0] == 1 and action != None:
            # action play: run_loop()
            # action quit: quit()
            action()
    else:
        pygame.draw.rect(screen, ic, (x,y,w,h))

    # text to display on the button
    textSurf, textRect = text_objects(msg, small_text,black)
    textRect.center = ( (x+(w/2)), (y+(h/2)) )
    screen.blit(textSurf, textRect)

# start game
def start_game():
    started = True
    while started:
        # set bg
        set_bg()

        # reigster events
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

        # set content
        TextSurf, TextRect = text_objects("SHOOT BUILDER", large_text, light_green)
        TextRect.center = ((SCREEN_WIDTH/2),(SCREEN_HEIGHT/2))
        screen.blit(TextSurf, TextRect)

        button("start",150,450,100,50,green,bright_green,run_game)
        button("exit",550,450,100,50,red,bright_red,quit_game)

        pygame.display.update()
        clock.tick(15)

# run game
def run_game():
    running = True
    while running:
        # set bg
        set_bg()

        # set initial score
        set_score()

        # register event
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    running = False

            elif event.type == QUIT:
                running = False

            elif event.type == ADDENEMY:
                new_enemy = Enemy()
                enemies.add(new_enemy)
                all_sprites.add(new_enemy)

            elif event.type == ADDCLOUD:
                new_cloud = Cloud()
                clouds.add(new_cloud)
                all_sprites.add(new_cloud)

        pressed_keys = pygame.key.get_pressed()
        player.update(pressed_keys)
        enemies.update()
        clouds.update()

        for entity in all_sprites:
            screen.blit(entity.surf, entity.rect)

        if pygame.sprite.spritecollideany(player, clouds):
            global score
            score +=  10
            move_down_sound.stop()
            collision_sound.play()
            set_score()
            remove_collieds()

            refresh_bullet_speed()

        if pygame.sprite.spritecollideany(player, enemies):
            move_up_sound.stop()
            move_down_sound.stop()
            collision_sound.play()

            player.kill()
            running = False

        pygame.display.flip()

        clock.tick(45)

# exit game
def quit_game():
    pygame.quit()
    quit()


pygame.mixer.init()
pygame.init()
clock = pygame.time.Clock()

screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))

ADDENEMY = pygame.USEREVENT + 1
#pygame.time.set_timer(ADDENEMY, random.randint(250, 750))
pygame.time.set_timer(ADDENEMY, bullet_speed)

ADDCLOUD = pygame.USEREVENT + 2
pygame.time.set_timer(ADDCLOUD, random.randint(500, 1000))

player = Player()
enemies = pygame.sprite.Group()
clouds = pygame.sprite.Group()
all_sprites = pygame.sprite.Group()
all_sprites.add(player)

# background sound
pygame.mixer.music.load("media/Apoxode_-_Electric_1.mp3")
pygame.mixer.music.play(loops=-1)

# action sounds
move_up_sound = pygame.mixer.Sound("media/Rising_putter.ogg")
move_down_sound = pygame.mixer.Sound("media/Falling_putter.ogg")
collision_sound = pygame.mixer.Sound("media/Collision.ogg")

# fonts
small_text = pygame.font.Font("freesansbold.ttf",20)
large_text = pygame.font.Font('freesansbold.ttf',80)

start_game()
pygame.mixer.music.stop()
pygame.mixer.quit()
